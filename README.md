Audiofield Info
===

About
---
audiofield_info is a simple formatter for use with the [Audiofield](http://drupal.org/project/audiofield) module. It uses the [getID3](http://drupal.org/project/getid3) to provide cached, quick metadata formatters for audio files, such as the duration of an MP3, or the ID3 tag title of the audio field, wherever field formatters are used.
